const { User_game } = require('../../models')
const { Op } = require("sequelize")

class UserController {
    getUser = (req, res) => {
        const name = req.query.name
        let whereCondition
        if (name) {
            whereCondition = {
                where: {
                    name: {
                        [Op.iLike]: `%${name}%`
                    }
                }
            }
        }
        User_game.findAll(whereCondition)
        .then(user => {
            res.result(user)
        })
    }

    getDetailuser = (req, res) => {
        User_game.findOne({
            where: { id: req.params.id }
        })
        .then(user => {
            res.result(user)
        })
    }

    insertUser = (req, res) => {
        User_game.create({
            name: req.body.name,
            username: req.body.username,
            age: req.body.age,
            password: req.body.password
        }).then(user => {
            res.result(user)
        }) .catch(err => {
            res.status(422).json("Can't create user")
        })
    }

    updateUser = (req, res) => {
        User_game.update({
            name: req.body.name,
            username: req.body.username,
            age: req.body.age,
            password: req.body.password
        }, {
        where: { id: req.params.id }
        })
        .then(user => {
            User_game.findOne({
                where: { id: req.params.id }
            })
            .then(user => {
                res.result(user)
            })
        }) .catch(err => {
            res.status(422).json("Can't update user")
        })
    }

    deleteUser = (req, res) => {
        User_game.destroy({
            where: {
                id: req.params.id
            }
        })
        .then(() => {
            res.result({})
        })
    }
}

module.exports = UserController