const { join } = require('path')
const { User_game } = require('../../models')
const bcrypt = require('bcrypt')

class AuthController {

    login = (req, res) => {
        res.render(join(__dirname, '../../views/login'))
    }

    doLogin = (req, res) => {
        const body = req.body

        if(!(body.username && body.password)) {
            return res.status(400).send({ error: "Username/Password salah" })
        }

        User_game.findOne({
            where: {username: body.username }
        })
        .then(user => {
            bcrypt.compare(body.password, user.password, (err, data) => {
                if (data) {
                    res.cookie('loginData', JSON.stringify(user))
                    res.redirect('/')
                } else {
                    return res.status(401).json({ msg: "User tidak ditemukan" })
                }
            })
        })
        .catch(err => {
            return res.status(401).json({ msg: "invalid credencial" })
        })
    }

    logout = (req, res) => {
        res.clearCookie('loginData')
        res.redirect('/')
    }
}

module.exports = AuthController