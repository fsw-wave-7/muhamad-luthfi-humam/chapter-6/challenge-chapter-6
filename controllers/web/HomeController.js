const { User_game } = require('../../models')
const { User_game_biodata } = require('../../models')
const { User_game_history } = require('../../models')
const { join } = require('path')
const bcrypt = require('bcrypt')

class HomeController {

    index = (req, res) => {
        User_game.findAll()
        .then(users => {
            res.render(join(__dirname, '../../views/index'), {
                content: './pages/userList',
                users: users
            })
        })    
    }

    userBiodata = (req, res) => {
        User_game_biodata.findAll()
        .then(users => {
            res.render(join(__dirname, '../../views/index'), {
                content: './pages/userBiodata',
                users: users
            })
        })    
    }

    userHistory = (req, res) => {
        User_game_history.findAll()
        .then(users => {
            res.render(join(__dirname, '../../views/index'), {
                content: './pages/userHistory',
                users: users
            })
        })    
    }

    add = (req, res) => {
        res.render(join(__dirname, '../../views/index'), {
            content: './pages/addList'
        })
    }

    saveUser = async (req, res) => {
        const salt = await bcrypt.genSalt(10)

        User_game.create({
            name: req.body.name,
            username: req.body.username,
            age: req.body.age,
            password: await bcrypt.hash(req.body.password, salt)
        })
        .then(user => {
            res.redirect('/')
        }) .catch(err => {
            console.log(err)
        })
    }

    editUser = (req, res) => {
        const index  = req.params.id 

        User_game.findOne({
            where: {id: index},
        }).then((user) => {
            res.render(join(__dirname, '../../views/index'), {
                content: './pages/userEdit',
                user: user
            })
        })
    }

    updateUser = (req, res) => {
        User_game.update({
            name: req.body.name,
            username: req.body.username,
            age: req.body.age,
            password: req.body.password
        }, {
        where: { id: req.params.id }
        })
        .then(user => {
            res.redirect('/')
        }) .catch(err => {
            res.status(422).json("Can't update user")
        })
    }

    deleteUser = (req, res) => {
        User_game.destroy({
            where: {
                id: req.params.id
            }
        })
        .then(() => {
            res.redirect('/')
        })
    }
}

module.exports = HomeController