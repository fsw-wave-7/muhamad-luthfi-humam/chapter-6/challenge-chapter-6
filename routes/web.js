const { Router } = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const AuthMiddleware = require('../middlewares/AuthMiddleware')

const AuthController = require('../controllers/web/AuthController')
const HomeController = require('../controllers/web/HomeController')

const web = Router()

web.use(bodyParser.json())
web.use(bodyParser.urlencoded({ extend: true }))
web.use(cookieParser())

const authController = new AuthController
const homeController = new HomeController

// login & logout
web.get('/login', authController.login)
web.post('/login', authController.doLogin)
web.get('/logout', authController.logout)

web.use(AuthMiddleware())

// create
web.get('/add', homeController.add)
web.post('/save-user', homeController.saveUser)

// read
web.get('/', homeController.index)
web.get('/user-biodata', homeController.index)
web.get('/user-history', homeController.index)


// update
web.get('/edit/:id', homeController.editUser)
web.post('/save-edit/:id', homeController.updateUser)

// delete
web.get('/delete/:id', homeController.deleteUser)

module.exports = web